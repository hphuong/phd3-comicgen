<?php

// Path to PHP-SDK
require '/Applications/XAMPP/xamppfiles/htdocs/src/facebook.php';

$facebook = new Facebook(array(
  'appId'  => '320641024695712',
  'secret' => '09284f41912377d7774392ca81066883',
  'fileUpload' => true,
));
 
// See if there is a user from a cookie
$user = $facebook->getUser();

if ($user) {
  try {
    // Proceed knowing you have a logged in user who's authenticated.
    $user_profile = $facebook->api('/me');
  } catch (FacebookApiException $e) {
    echo '<pre>'.htmlspecialchars(print_r($e, true)).'</pre>';
    $user = null;
  }

  $access_token = $facebook->getAccessToken();

  $photo = 'youtube/stitched.JPG';
  $message = '@ the Dropbox hackathon w free food!';
  $attachment = array(
            // 'message' => $message,
            // 'link' => "http://twitter.com/#!/search/%23PHD3",
            // 'type'=>'photo',
            'source'=> '@'.$photo,
            // 'access_token' =>$access_token,
    );

	try {
	  $facebook->api('/me/photos', 'POST', $attachment);
	}
	catch (FacebookApiException $e) {
	        error_log('Could not post image to Facebook.');
	      }
}else{
	//echo'Not logged in';
}

?>

<!DOCTYPE html>
<html xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
    <title>Facebook Application Local Development</title>
    <link type="text/css" rel="stylesheet" href="css/reset.css">
    <link type="text/css" rel="stylesheet" href="css/main.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
</head>
  <body>
    <div id="login">
        <?php if (!$user) { ?>
          <fb:login-button scope="read_stream,publish_stream,user_photos,photo_upload"></fb:login-button>
        <?php } ?>
    </div>
    <div id="cont">
        <?php if ($user): ?>
        <img src="https://graph.facebook.com/<?php echo $user; ?>/picture">
        <p>Comic uploaded!</p>
 
        <?php else: ?>
        <strong><em>You are not Connected.</em></strong>
        <?php endif ?>
    </div>
    <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId: '<?php echo $facebook->getAppID() ?>',
          cookie: true,
          xfbml: true,
          oauth: true
        });
        FB.Event.subscribe('auth.login', function(response) {
          window.location.reload();
        });
        FB.Event.subscribe('auth.logout', function(response) {
          window.location.reload();
        });
      };
      (function() {
        var e = document.createElement('script'); e.async = true;
        e.src = document.location.protocol +
          '//connect.facebook.net/en_US/all.js';
        document.getElementById('fb-root').appendChild(e);
      }());
    </script>
  </body>
</html>


