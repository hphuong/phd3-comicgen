  <?php


	if ($_SERVER['REQUEST_METHOD'] == 'GET')
	{
		$url = (isset($_GET['videofile']) && !empty($_GET['videofile'])) ? $_GET['videofile'] : false;
		if (!$url) {
			$arr_output = array('error' => "Please enter a URL.");
			echo json_encode($arr_output);
		} else {

		$imgOut=getFrame($_GET['videofile'],$_GET['timestamp'],$_GET['index']);
		$dims = getimagesize($imgOut);
		$image_res = imagecreatefromjpeg($imgOut);
		$width = $dims[0];
		$height = $dims[1];
		$new_width = 400;
		$ratio = $width/$new_width;
		$new_height = (int) $height/$ratio;
		$result = imagecreatetruecolor($new_width, $new_height);
		
		imagecopyresized($result, $image_res, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

		imagejpeg($result, $imgOut);

			$arr_output = array('result'=> 'success', 'filename' => $imgOut);
			echo json_encode($arr_output);
		}
	}


  function getFrame($videoFile,$second,$frameNum)
  {
        // Full path to ffmpeg (make sure this binary has execute permission for PHP)
		$ffmpeg = "/Applications/XAMPP/xamppfiles/htdocs/ffmpeg";
		 
		// // Full path to the video file
		//$videoFile = "/Applications/XAMPP/xamppfiles/htdocs/that_gotye_song.mp4";
		 
		// // Full path to output image file (make sure the containing folder has write permissions!)
		$imgOut = str_replace('.', "_", $videoFile."_".$second).".jpg";
		 
		// // Number of seconds into the video to extract the frame
		//$second = 10;

		 
		// // Setup the command to get the frame image
		$cmd = $ffmpeg." -i \"".$videoFile."\" -an -ss ".$second." -t 00:00:1 -r 1 -y -f mjpeg \"".$imgOut."\" 2>&1";
		 
		


		// Get any feedback from the command
		 $feedback = `$cmd`;
		 //echo $feedback;
		 return $imgOut;
	}

?>