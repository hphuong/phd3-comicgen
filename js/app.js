$(function(){

   $('#status').hide();
   $('#snapshot_button').hide();
   $('#done_button').hide();
   $('#editor').hide();
   $("#youtubeinput").keypress(function(event) {
      if ( event.which == 13 ) {
         if ($("#youtubeinput").val() == "") {

         } else {
            start_loader();

         }
      }
   });

   function start_loader(){
      event.preventDefault();
      window.video_id = youtube_parser($("#youtubeinput").val());
      $('#status').show();
      $('#status').removeClass().addClass("alert");
      $('#status').html("Loading Video");
      $('#status').before('<br><p align="center"><img src="img/loading.gif" alt="" id="loading"></p>')
      $("#youtubeplayer").empty();

      $.getJSON('Youtube/getVideo.php?url='+window.video_id, function(data) {
         youtube_loader();
         window.saved_video = data['filename'];
         window.image_urls = [];
         window.time_stamps = [];
         window.timeStampQueue = [];
         window.captions = [];
         $('#timestamps').empty();
         $('#status').show();
         $('#status').removeClass().addClass("alert alert-success");
         $('#status').html("Successfully loaded: "+data['title']);
         $('#snapshot_button').show();
         $('#done_button').show();
         $('#loading').remove();
      });
   }

   function youtube_loader() {
      var template_json = {"video_id": video_id};  
      var template = _.template($("#player-template").html());
      $("#youtubeplayer").empty().append(template(template_json));
   }


   function youtube_parser(url){
       var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
       var match = url.match(regExp);
       if (match&&match[7].length==11){
           return match[7];
       }else{
           alert("Url incorrecta");
       }
   }

   $('#restart_button').click(function(event) {
         event.preventDefault();
         $('#editor').hide();
         $('#status').html("Please insert a new youtube video");
         $('#editor').hide();
         $(".thumbsnails").empty();
         $("#youtubeplayer").empty();
         $("#capture").show();
         start_loader();
   });



});

function imageExtractAddQueue(timestamp){
   window.timeStampQueue.push(timestamp);
   imageExtractJquery();
}

function imageExtractJquery(){
   if (window.timeStampQueue.length > 0) {
      var time = window.timeStampQueue.pop();
      $.getJSON('youtube/getFrame.php?videofile='+window.saved_video+"&timestamp="+ time, function(data) {
         window.image_urls.push(data['filename']);
         //$('#status').html(data);
         $('#status').html("Added Frame at: "+time);

         $('#preview').html('<br><div class="thumbnail"><img src="YouTube/'+data['filename']+'" alt=""><br></div>');
         imageExtractJquery();
      });
   } 
}
function onYouTubePlayerReady(playerId) {
   player = document.getElementById("myytflashplayer");
   player.playVideo();

   $('#snapshot_button').click(function() {
      var time = Math.round(player.getCurrentTime()*100)/100;
      window.time_stamps.push(time);
      imageExtractAddQueue(time);
      //player.pauseVideo();
      //$('#timestamps').append(player.getCurrentTime() + ", ");   
      $('#status').html("Loading Frame");
   });

   // $('#snapshot_button').mouseup(function() {
   //    player.playVideo();
   // });

   $('#done_button').click(function() {
      $('#editor').hide();
      $('#status').hide();
      $('#snapshot_button').hide();
      $('#done_button').hide();
      $('#editor').show();
      $("#youtubeplayer").hide();
      $("#capture").hide();
      $("#preview").hide();


      $(".thumbnails").empty().show();
      for (i in window.image_urls) {
         var template_json = {"filename": window.image_urls[i],"id": i, "name":window.image_urls[i]};
         var template = _.template($("#image-template").html());
         $(".thumbnails").append(template(template_json));

      }

      $('#editor').find('img').each(function(){
         $(this).bind('mouseup',function(e){ 
            //var caption = $(this).val();
            var index = parseInt($(this).parent().find('input').attr("id").substring(6));
            var caption = $(this).parent().find('input').val();
            
            var relativeX = e.pageX - $(this).position()['left'];
            var relativeY = e.pageY - $(this).position()['top'];

            doImageEdit(window.image_urls[index], relativeX, relativeY, caption, 10, this, index);

         });
      });


   });
   
   $('#process_button').click(function(event) {
      event.preventDefault();
      doStitch(window.image_urls);

   });

}  


function doImageEdit(filename, x, y, text, fontsize, jquery_this, index) {
   $.ajax({
      url: "youtube/editImage.php",
      type: "GET",
      data: {filename: filename, x: x, y: y, text: text, fontsize: fontsize},
      contentType: "application/json",
      dataType: "json",
      success: function(filename)
      {
         console.log(filename);
         $(jquery_this).prop("src","youtube/"+filename);
         window.image_urls[index] = filename;
      },
      error: function(xhr, textStatus, errorThrown)
      {
         alert("An error occured");
      }
   });
}

function doStitch(filenames)
{  
   var str = JSON.stringify(filenames);
   $.ajax({
      url: "youtube/stitchImages.php",
      type: "GET",
      data: "image_files="+str+"&filename="+window.video_id,
      contentType: "application/json",
      dataType: "json",
      success: function(filename)
      {
         console.log(filename);

         $("#capture").hide();
         $("#editor").hide();

         var template_json = {"filename": filename};
         var template = _.template($("#final-template").html());
         $("#final").html(template(template_json));
         $("#fb_button").click(function(e){
            e.preventDefault();
            facebook();
         });
            

      },
      error: function(xhr, textStatus, errorThrown)
      {
         alert("An error occured");
         console.log(errorThrown);
         console.log(textStatus);
         console.log(xhr);
      }
   });
}

function facebook()
{  
   $('#fb_button').after('<br><p align="center"><img src="img/loading.gif" alt="" id="loading"></p>');
   $.ajax({
      url: "fb.php",
      type: "GET",
      data: "",
      contentType: "application/json",
      dataType: "json",
      success: function(filename)
      {
         $('#loading').remove();
         $("#fb_button").after("<h3>Posted</h3>");

      },
      error: function(xhr, textStatus, errorThrown)
      {
         $('#loading').remove();
         $("#fb_button").after("<h3>Posted</h3>");
      }
   });
}




